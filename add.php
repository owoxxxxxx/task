<?php
    require_once('insert.php');

    if(isset($_POST['type']))
    {
        $newProduct = $_POST['type'];
        $product = new $newProduct();
        $product->setSharedProperties($_POST['SKU'],$_POST['Name'],$_POST['Price']);

        $product->connectToDatabase();
        $product->setProduct();
        $product->settype();
        $product->redirectToHomepage();
        $product->disconnectFromDatabase();
    }
?>


<!DOCTYPE html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="css.css">
    <title>Product Add</title>
</head>
<body>

<div class="class mt-3"></div>

<div class="d-flex ml-5 mr-5">
     
  <div class="mr-auto p-2 ml-3"><a class="navbar-brand">Product Add</a></div>
  <button type="submit" form="product_form" class="btn btn-primary text-light mx-2">Save</a></button>
  <div class="p-2"><a class="btn btn-danger p-2 ml-4 mr-4" href="./" role="button">Cancel</a></div>
</div>

<div class="class mt-3"></div>

<div class="border border-dark ml-5 mr-5"></div>




<div class="flex-container d-flex justify-content-center">
     <form method="post" id="product_form" >

          <div class="row my-4 mx-1">
               
               <label for="sku" class="col-form-label" style="width: 5rem;">SKU</label>
               <input type="text" id="sku" class="form-control d-inline" name="sku" placeholder="SKU" required="true" style="width: 15rem;">
          </div>

          <div class="row my-4 mx-1">
               <label for="name" class="col-form-label" style="width: 5rem;">Name</label>
               <input type="text" id="name" class="form-control d-inline" name="name" placeholder="Name" required="true" style="width: 15rem;">
          </div>

          <div class="row my-4 mx-1">
               <label for="price" class="col-form-label" style="width: 5rem;">Price ($)</label>
               <input type="text" id="price" class="form-control d-inline" name="price" placeholder="0.00" required="true"  maxlength="6" style="width: 15rem;">
          </div>

          <div class="my-3 mx-1">
               <select onchange="yesnoCheck(this);"                  id="productType" class="custom-select text-center" aria-label="ProductType" name="type" style="width: 10rem;" required >
                    <option disabled selected value>Type Switcher</option>
                    <option class="dropdown-item" id="DVD" value="DVD">DVD</option>
                    <option class="dropdown-item" id="Furniture" value="Furniture">Furniture</option>
                    <option class="dropdown-item" id="Book" value="Book">Book</option>
               </select>
          </div>














          <div class="row my-3 mx-1">
                            <div id="specialAttribute">

                                <div id="DVDProperties">
                                    <label for="size" class="col-form-label" style="width: 6rem;">Size (MB) </label>
                                    <input type="text" id="size" class="form-control d-inline" name="Size" placeholder="0" required="true"  maxlength="4" style="width: 12rem;">
                                </div>

                                <div id="FurnitureProperties">
                                    <div class="row my-3 mx-1">
                                        <label for="height" class="col-form-label" style="width: 6rem;">Height (cm) </label>
                                        <input type="text" id="height" class="form-control d-inline" name="Height" placeholder="0" required="true"  maxlength="4" style="width: 12rem;">
                                    </div>

                                    <div class="row my-3 mx-1">
                                        <label for="width" class="col-form-label" style="width: 6rem;">Width (cm) </label>
                                        <input type="text" id="width" class="form-control d-inline" name="Width" placeholder="0" required="true"  maxlength="4" style="width: 12rem;">
                                    </div>

                                    <div class="row my-3 mx-1">
                                        <label for="length" class="col-form-label" style="width: 6rem;">Length (cm) </label>
                                        <input type="text" id="length" class="form-control d-inline" name="Length" placeholder="0" required="true"  maxlength="4" style="width: 12rem;">
                                    </div>
                                </div>

                                <div id="BookProperties">
                                    <label for="weight" class="col-form-label" style="width: 6rem;">Weight (kg) </label>
                                    <input type="text" id="weight" class="form-control d-inline" name="Weight" placeholder="0" required="true" style="width: 12rem;">
                                </div>
                            </div>
                        </div>

                        <div class="font-weight-bold" id="product_description"></div>


 <!--tester-->
     <!--kad izvēlās opcijas-->
     
<div id="adc" style="display: none;">

     <div class="row my-4 mx-1">
          <label for="DVD" class="col-form-label" style="width: 5rem;">Size&nbsp(MB)</label>
          <input type="text" id="DVD" class="form-control d-inline ml-4" name="DVD" placeholder="Size" required="true" style="width: 15rem;">
     </div>

</div>
<div id="pc" style="display: none;">

     <div class="row my-4 mx-1">
          <label for="DVD" class="col-form-label" style="width: 5rem;">Height&nbsp(CM)</label>
          <input type="text" id="Furniture" class="form-control d-inline ml-4" name="Furniture" placeholder="Height" required="true" style="width: 15rem;">
     </div>

     <div class="row my-4 mx-1">
          <label for="DVD" class="col-form-label" style="width: 5rem;">Width&nbsp(CM)</label>
          <input type="text" id="Furniture" class="form-control d-inline d-inline ml-4" name="Furniture" placeholder="Width" required="true" style="width: 15rem;">
     </div>

     <div class="row my-4 mx-1">
          <label for="DVD" class="col-form-label" style="width: 5rem;">Length&nbsp(CM)</label>
          <input type="text" id="Furniture" class="form-control d-inline d-inline ml-4" name="Furniture" placeholder="Length" required="true" style="width: 15rem;">
     </div>

</div>
<div id="ps" style="display: none;">

     <div class="row my-4 mx-1">
          <label for="Book" class="col-form-label" style="width: 5rem;">Weight&nbsp(KG)</label>
          <input type="text" id="Book" class="form-control d-inline ml-4" name="Book" placeholder="Weight" required="true" style="width: 15rem;">
     </div>

</div>
     <!--kad izvēlās opcijas-->


     </form>
</div>
<!--input beigas-->



<!--__________________-->
<script src="add.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>