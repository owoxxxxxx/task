// #region Variables
var productdescription;
var buttonName;
var lastAttribute;
// #endregion

// #region Disabling special property input fields
document.getElementById("size").disabled = "true";
document.getElementById("height").disabled = "true";
document.getElementById("width").disabled = "true";
document.getElementById("length").disabled = "true";
document.getElementById("weight").disabled = "true";
// #endregion

// Adds an event listener for when the product type is changed
document.getElementById("productType").addEventListener("change", function() { dropdown(document.getElementById("productType").value); } );

function dropdown(currentAttribute)
{
    currentAttribute = currentAttribute + "Properties"; // Adds the suffix "Properties" to the end of the attribute (to get correct container elements for the input fields)

    if (typeof lastAttribute !== 'undefined') // Checks if lastAttribute is defined (a value has been selected beforehand)
    {
        document.getElementById(lastAttribute).style.display = "none"; // Hides the last selected product type

        for(var propertyElement = 0; propertyElement <  document.getElementById(lastAttribute).getElementsByTagName('input').length; propertyElement++) // Loops through all the special property fields (furniture has 3)
        {
            document.getElementById(lastAttribute).getElementsByTagName('input')[propertyElement].disabled = true; // Disables the last special property field selected
        }
    }
    lastAttribute = currentAttribute; // Sets lastAttribute as the currently selected product type 

    switch (currentAttribute)
    {
        case "DVDProperties": // DVD product type
            document.getElementById("DVDProperties").style.display = "inline-block"; // Displays the DVD input field and label
            document.getElementById("size").removeAttribute("disabled");
            productdescription = "Please provide the size in MB."; // Sets the description to be relevant for the DVD product type
            break;
        case "FurnitureProperties": // Furniture product type
            document.getElementById("FurnitureProperties").style.display = "inline-block"; // Displays the furniture input fields and labels
            document.getElementById("height").removeAttribute("disabled");
            document.getElementById("width").removeAttribute("disabled");
            document.getElementById("length").removeAttribute("disabled");
            productdescription = "Please provide the dimensions in cm."; // Sets the description to be relevant for the furniture product type
            break;
        case "BookProperties": // Book product type
            document.getElementById("BookProperties").style.display = "inline-block"; // Displays the book input field and label
            document.getElementById("weight").removeAttribute("disabled");
            productdescription = "Please provide the weight in kg."; // Sets the productdescription variable to be relevant for the book product type
            break;
    }
    document.getElementById("product_description").innerHTML = productdescription; // Displays the product description according to the selected product type
}



function yesnoCheck(that) 
{
    if (that.value == "DVD") 
    {
        document.getElementById("adc").style.display = "block";
    }
    else
    {
        document.getElementById("adc").style.display = "none";
    }
    if (that.value == "Furniture")
    {
        document.getElementById("pc").style.display = "block";
    }
    else
    {
        document.getElementById("pc").style.display = "none";
    }
    if (that.value == "Book")
    {
        document.getElementById("ps").style.display = "block";
    }
    else
    {
        document.getElementById("ps").style.display = "none";
    }
}

