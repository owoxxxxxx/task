<?php

abstract class Product
{
    #region Shared Variables
    protected $sku;
    protected $name;
    protected $price;
    protected $type;

    protected $productID;
    #endregion

    #region SQL Database vairables
    protected $servername = "localhost";
    protected $username = "root";
    protected $password = "";
    protected $dbname = "db";

    protected $dbConnect;
    #endregion

    // Declares the abstract methods needed to assign values to the variables (setProperties) and insert the product into the database (settype)
    abstract function setProperties($propertyArray);
    abstract function settype();

    // Declares the abstract method used to get the special product properties (pre-formatted)
    abstract function getProduct();

    // Setter for the main Product class (contains all the shared properties between all product types)
    function setSharedProperties($sku, $name, $price)
    {
        // Sets the SKU, name and price of the product using POST, getting the values from the submitted form
        $this->sku = $sku;
        $this->name = $name; 
        $this->price = $price;
    }

    // Method to connect to the database
    function connectToDatabase()
    {
        $this->dbConnect = new mysqli($this->servername, $this->username, $this->password, $this->dbname);
    }

    // Method to close the connection to the database
    function disconnectFromDatabase()
    {
        $this->dbConnect->close();
    }

    // Method to insert the product into the products table
    function setProduct()
    {
        // Prepares the statement to insert the product
        $stmt = $this->dbConnect->prepare("INSERT INTO products (sku, `name`, price, `type`) VALUES (?,?,?,?)");
        // Binds the shared variables to the prepared statement
        $stmt->bind_param("ssds", $this->sku, $this->name, $this->price, $this->type);
        // Executes the statement with the bound values
        $stmt->execute();

        // Assigns a value to the productID variable, which is the ID of the product that was just inserted into the products database
        $this->productID = $this->dbConnect->insert_id;
    }

    // Method to redirect back to the homepage
    function redirectToHomepage()
    {
        header("Location: ../");
    }
}

class DVD extends Product
{
    // Initializes the variable unique to the DVD product type
    protected $size;

    function setProperties($propertyArray) // Assigns values to the necessary variables for a DVD type product
    {
        if(isset($_POST['Size']))
        {
            // Gets the Size of the DVD from the filled form
            $this->size = $_POST['Size'];
            // Sets the type to DVD (the abstract class name)
            $this->type = 'DVD';
        }
        else
        {
            $this->productID = $propertyArray['product_id'];
            $this->size = $propertyArray['size'];
        }
    }

    function settype()
    {
        // Prepares the statement to insert the unique product variables
        $stmt = $this->dbConnect->prepare("INSERT INTO dvds (dvd_id, size) VALUES (?, ?)");
        // Binds the productID and the unique variable to the prepared statement
        // Executes the prepared statement with the bound values
        $stmt->execute();
    }

    function getProduct()
    {
        echo '
        <div class="col-xl-3 col-lg-4 col-sm-6 col-12">
            <div class="card text-center my-4 mx-4">
            <input name="checkbox[]" class="delete-checkbox form-check-input ml-3 mt-3" type="checkbox" form="deleteForm" value="'. $this->productID .'">
                <div class="mt-4 mb-3">
                    <div class="card-text">'. $this->sku .'</div>
                    <div class="card-text">'. $this->name .'</div>
                    <div class="card-text">$ '. $this->price .'</div>
                    <div class="card-text text-nowrap">Size: '. $this->size .' MB</div>
                </div>
            </div>
        </div>';
    }
}

class Furniture extends Product
{
    // Initializes the variables unique to the Furniture product type
    protected $height;
    protected $width;
    protected $length;

    function setProperties($propertyArray) // Assigns values to the necessary variables for a Furniture type product
    {
        if(isset($_POST['Height']))
        {
            // Sets the height, width and length of the furniture piece from the filled form
            $this->height = $_POST['Height'];
            $this->width = $_POST['Width'];
            $this->length = $_POST['Length'];
            // Sets the type to the Furniture (the abstract class name)
            $this->type = 'Furniture';
        }
        else
        {
            $this->productID = $propertyArray['product_id'];
            $this->height = $propertyArray['height'];
            $this->width = $propertyArray['width'];
            $this->length = $propertyArray['length'];
        }
    }

    function settype()
    {
        // Prepares the statement to insert the unique product variables
        $stmt = $this->dbConnect->prepare("INSERT INTO furniture (furniture_id, height, width, length) VALUES (?, ?, ?, ?)");
        // Binds the productID and the unique variables to the prepared statement
        $stmt->bind_param("iiii", $this->productID, $this->height, $this->width, $this->length);
        // Executes the prepared statement with the bound values
        $stmt->execute();
    }

    function getProduct()
    {
        echo '
        <div class="col-xl-3 col-lg-4 col-sm-6 col-12">
            <div class="card text-center my-4 mx-4">
            <input name="checkbox[]" class="delete-checkbox form-check-input ml-3 mt-3" type="checkbox" form="deleteForm" value="'. $this->productID .'">
                <div class="mt-4 mb-3">
                    <div class="card-text">'. $this->sku .'</div>
                    <div class="card-text">'. $this->name .'</div>
                    <div class="card-text">$ '. $this->price .'</div>
                    <div class="card-text text-nowrap">Dimensions: '. $this->height . 'x' . $this->width . 'x' . $this->length . '</div>
                </div>
            </div>
        </div>';
    }
}

class Book extends Product
{
    // Initializes the variable unique to the Book product type
    protected $weight;

    function setProperties($propertyArray) // Assigns values to the necessary variables for a Book type product
    {
        if(isset($_POST['Weight']))
        {
            // Sets the weight of the book from the filled form
            $this->weight = $_POST['Weight'];
            // Sets the type to Book (the abstract class name)
            $this->type = 'Book';
        }
        else
        {
            $this->productID = $propertyArray['product_id'];
            $this->weight = $propertyArray['weight'];
        }
    }

    function settype()
    {
        // Prepares the statement to insert the unique product variables
        $stmt = $this->dbConnect->prepare("INSERT INTO books (book_id, weight) VALUES (?, ?)");
        // Binds the productID and the unique variable to the prepared statement
        $stmt->bind_param("id", $this->productID, $this->weight);
        // Executes the prepared statement with the bound values
        $stmt->execute();
    }

    function getProduct()
    {
        echo '
        <div class="col-xl-3 col-lg-4 col-sm-6 col-12">
            <div class="card text-center my-4 mx-4">
                <input name="checkbox[]" class="delete-checkbox form-check-input ml-3 mt-3" type="checkbox" form="deleteForm" value="'. $this->productID .'">
                <div class="mt-4 mb-3">
                    <div class="card-text">'. $this->sku .'</div>
                    <div class="card-text">'. $this->name .'</div>
                    <div class="card-text">$ '. $this->price .'</div>
                    <div class="card-text text-nowrap">Weight: '. $this->weight .' kg</div>
                </div>
            </div>
        </div>';
    }
}

?>
